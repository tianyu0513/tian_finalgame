﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public GameObject happinessText;
    public GameObject hungerText;
    public GameObject cleanlinessText;
    public GameObject iqText;

    public GameObject namePanel;
    public GameObject nameInput;
    public GameObject nameText;

    public GameObject robot;
    public GameObject robotPanel;
    public GameObject[] robotList;

    public GameObject homePanel;
    public Sprite[] homeTileSprites;
    public GameObject[] homeTiles;

    public GameObject background;
    public Sprite[] backgroundOptions;

    public GameObject foodPanel;
    public Sprite[] foodIcons;

    public GameObject cleanPanel;
    public Sprite[] CleanIcons;

    public GameObject studyPanel;
    public Sprite[] StudyIcons;

    public GameObject happinessPanel;
    public Sprite[] HappinessIcons;

    public GameObject endPanel;
    public Text _endText;

    int day = 0;
    public GameObject newDayPanel;
    public Text dayCount;


    int actionCount = 0;
    public Text _actionCount;

    public Text _specialEventText;

    bool sickDay;
    bool cleanDay;
    bool noeatDay;
    bool happyDay;

    private bool restart;

    void Awake()
    {
        newDay();
        //restart = false;
        //GameOver();
    }
    void Start()
    {
        if (!PlayerPrefs.HasKey("looks"))
            PlayerPrefs.SetInt("looks", 0);
        createRobot(PlayerPrefs.GetInt("looks"));

        if (!PlayerPrefs.HasKey("tiles"))
            PlayerPrefs.SetInt("tiles", 0);
        changeTiles(PlayerPrefs.GetInt("tiles"));

        if (!PlayerPrefs.HasKey("background"))
            PlayerPrefs.SetInt("background", 0);
        changeBackground(PlayerPrefs.GetInt("background"));
        restart = false;
        //newDay();
        //GameOver();
    }

    void Update () {
        happinessText.GetComponent<Text>().text = "" + robot.GetComponent<Robot>().happiness;
        hungerText.GetComponent<Text>().text = "" + robot.GetComponent<Robot>().hunger;
        cleanlinessText.GetComponent<Text>().text = "" + robot.GetComponent<Robot>().cleanliness;
        iqText.GetComponent<Text>().text = "" + robot.GetComponent<Robot>().iq;
        nameText.GetComponent<Text>().text = robot.GetComponent<Robot>().name;
        _actionCount.text = "AP: " + actionCount;
        if (restart)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
    }
   
    public void triggerNamePanel(bool b)
    {
        namePanel.SetActive(!namePanel.activeInHierarchy);

        if (b)
        {
            robot.GetComponent<Robot>().name = nameInput.GetComponent<InputField>().text;
            PlayerPrefs.SetString("name", robot.GetComponent<Robot>().name);
        }
    }

    public void buttonBehavior(int i)
    {
        switch (i)
        {
            case (0):
            default:
                robotPanel.SetActive(!robotPanel.activeInHierarchy);
                break;

            case (1):
                homePanel.SetActive(!homePanel.activeInHierarchy);
                break;

            case (2):
                foodPanel.SetActive(!foodPanel.activeInHierarchy);
                break;

            case (3):
                cleanPanel.SetActive(!cleanPanel.activeInHierarchy);
                break;

            case (4):
                studyPanel.SetActive(!studyPanel.activeInHierarchy);
                break;

            case (5):
                newDay();
                break;

            case (6):
                happinessPanel.SetActive(!happinessPanel.activeInHierarchy);
                break;

        }
    }

    public void newDay()
    {
        newDayPanel.SetActive(!newDayPanel.activeInHierarchy);
        day++;
        dayCount.text = "Day " + day;
        robot.GetComponent<Robot>().updateHunger(-15);
        robot.GetComponent<Robot>().updateCleanliness (-15);
        robot.GetComponent<Robot>().updateHappiness (-10);
        actionCount = 10;

        if(day == 9 || day ==12)
        {
            sickDay = true;
            _specialEventText.text = "sick day, no iq.";
        }
        
        else if(day % 5 == 0)
        {
            cleanDay = true;
            _specialEventText.text = "cleanday double cleanliness";
        }

        else if(day % 7 == 0)
        {
            noeatDay = true;
            _specialEventText.text = "you don't want to eat anything today";
        }

        else if (day == 3 || day == 18 || day == 24)
        {
            happyDay = true;
            _specialEventText.text = "you are super happy today, double happiness";
        }

        else
        {
            sickDay = false;
            cleanDay = false;
            noeatDay = false;
            happyDay = false;
            _specialEventText.text = "normal day";
        }

    }

    public void GameOver()
    {
        restart = true;
        endPanel.SetActive(!endPanel.activeInHierarchy);
        _endText.text = "You died! You have survived for " + day + " days";
    }
    public void createRobot (int i)
    {
        if (robot)
            Destroy(robot);
        robot = Instantiate(robotList[i], Vector3.zero, Quaternion.identity) as GameObject;

        toggle(robotPanel);

        PlayerPrefs.SetInt("looks", i);
    }

    public void changeTiles(int t)
    {
        for (int i = 0; i < homeTiles.Length; i++)
            homeTiles[i].GetComponent<SpriteRenderer>().sprite = homeTileSprites[t];

        toggle(homePanel);

        PlayerPrefs.SetInt("tiles", t);

    }
    
    public void changeBackground(int i)
    {
        background.GetComponent<SpriteRenderer>().sprite = backgroundOptions[i];

        toggle(homePanel);

        PlayerPrefs.SetInt("background", i);
    }

    public void selectFood(int i)
    {
        print("food selected:" + i);
        if (actionCount > 0)
        { 
            if (noeatDay == false)
            {
                if (day % 7 != 0)
                {
                    if (i == 0)
                    {
                        robot.GetComponent<Robot>().updateHunger(5);
                        actionCount--;
                    }
                    if (i == 1)
                    {
                        robot.GetComponent<Robot>().updateHunger(10);
                        robot.GetComponent<Robot>().updateIQ(-2);
                        actionCount -= 1;
                    }
                }

                else if (day % 7 == 0)
                {
                    if (i == 0)
                    {
                        robot.GetComponent<Robot>().updateHunger(0);
                        actionCount--;
                    }
                    if (i == 1)
                    {
                        robot.GetComponent<Robot>().updateHunger(0);
                        robot.GetComponent<Robot>().updateIQ(-2);
                        actionCount -= 1;
                    }
                }
            }
        }

            //toggle(foodPanel);

            //actionCount--;

        
    }

    public void selectClean(int i)
    {
        print("clean selected:" + i);
        if (actionCount > 0)
        {
            if (cleanDay == false)
            {
                if (day % 5 != 0)
                {
                    if (i == 0)
                    {
                        robot.GetComponent<Robot>().updateCleanliness(2);
                        actionCount--;
                    }
                    if (i == 1)
                    {
                        robot.GetComponent<Robot>().updateCleanliness(4);
                        robot.GetComponent<Robot>().updateHunger(-5);
                        actionCount -= 1;
                    }
                    //actionCount--;
                    //toggle(cleanPanel);
                }

                else if (day % 5 == 0)
                {
                    if (i == 0)
                    {
                        robot.GetComponent<Robot>().updateCleanliness(4);
                        actionCount--;
                    }
                    if (i == 1)
                    {
                        robot.GetComponent<Robot>().updateCleanliness(8);
                        robot.GetComponent<Robot>().updateHunger(-5);
                        actionCount -= 1;
                    }
                }
            }
        }
    }

    public void selectStudy(int i)
    {
        print("study selected:" + i);
        if (actionCount > 0)
        {
            if (sickDay == false)
            {
                if ((day != 9 || day != 12))
                {
                    if (i == 0)
                    {
                        robot.GetComponent<Robot>().updateIQ(5);
                        actionCount--;
                    }
                    if (i == 1)
                    {
                        robot.GetComponent<Robot>().updateIQ(10);
                        robot.GetComponent<Robot>().updateHappiness(-10);
                        actionCount -= 1;
                    }
                    //actionCount -= 2;
                    //toggle(studyPanel);
                }

                else if ((day == 9 || day == 12))
                {
                    if (i == 0)
                    {
                        robot.GetComponent<Robot>().updateIQ(0);
                        actionCount--;
                    }
                    if (i == 1)
                    {
                        robot.GetComponent<Robot>().updateIQ(0);
                        robot.GetComponent<Robot>().updateHappiness(-10);
                        actionCount -= 1;
                    }
                }
            }
        }
    }

    public void selectHappiness(int i)
    {
        if (actionCount > 0)
        {
            if(happyDay == false)
            {
                if (day != 3 || day != 18 || day != 24)
                {
                    if (i == 0)
                    {
                        robot.GetComponent<Robot>().updateHappiness(5);
                        actionCount--;
                    }
                    if (i == 1)
                    {
                        robot.GetComponent<Robot>().updateHappiness(10);
                        robot.GetComponent<Robot>().updateHunger(-5);
                        actionCount -= 1;
                    }
                }
                else if (day == 3 || day == 18 || day == 24)
                {
                    if (i == 0)
                    {
                        robot.GetComponent<Robot>().updateHappiness(10);
                        actionCount--;
                    }
                    if (i == 1)
                    {
                        robot.GetComponent<Robot>().updateHappiness(20);
                        robot.GetComponent<Robot>().updateHunger(-5);
                        actionCount -= 1;
                    }
                }
            }
        }
    }

    public void toggle(GameObject g)
    {
        if (g.activeInHierarchy)
            g.SetActive(false);
    }




}
