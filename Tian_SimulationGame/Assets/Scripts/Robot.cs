﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Robot : MonoBehaviour {

    [SerializeField]
    private int _hunger;
    [SerializeField]
    private int _happiness;
    [SerializeField]
    private int _cleanliness;
    [SerializeField]
    private int _iq;


    GameManager gameManager;


    [SerializeField]
    private string _name;

    //private bool _serverTime;
    private int _clickCount = 0;

	// Use this for initialization
	void Start () {
        //PlayerPrefs.SetString("then", "11/15/2018 15:29:12");
        //PlayerPrefs.SetString("then", getStringTime());
        setstart();
        updateStatus();
        if (!PlayerPrefs.HasKey("name"))
            PlayerPrefs.SetString("name", "Robot");
        _name = PlayerPrefs.GetString("name");

        gameManager = GameObject.FindGameObjectWithTag("manager").GetComponent<GameManager>();
	}
	
	// Update is called once per frame
	void Update () {

        GetComponent<Animator>().SetBool("jump", gameObject.transform.position.y > -2f);

        if (Input.GetMouseButtonUp(0))
        {
            //updateHunger(-1); 
            //Debug.Log("Clicked");
            Vector2 v = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(v), Vector2.zero);
            if (hit)
            {
                //Debug.Log(hit.transform.gameObject.name);
                if (hit.transform.gameObject.tag == "Robot")
                {
                    _clickCount++;
                    if (_clickCount ==1)//>=2
                    {
                        _clickCount = 0;
                        //updateHappiness(1);
                        GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 1000000));
                    }
                }

            }
        }
        
	}
    void setstart()
    {
        _hunger = 70;
        _happiness = 80;
        _cleanliness = 90;
        _iq = 20;
        PlayerPrefs.DeleteAll();
    }
    void updateStatus()
    {
        if (!PlayerPrefs.HasKey("_hunger"))
        {
            //_hunger = 100;
            PlayerPrefs.SetInt("_hunger", _hunger);
        }
        else
        {
            _hunger = PlayerPrefs.GetInt("_hunger");
        }

        if (!PlayerPrefs.HasKey("_happiness"))
        {
            //_happiness = 100;
            PlayerPrefs.SetInt("_happiness", _happiness);
        }
        else
        {
            _happiness = PlayerPrefs.GetInt("_happiness");
        }

        if (!PlayerPrefs.HasKey("_cleanliness"))
        {
            //_cleanliness = 100;
            PlayerPrefs.SetInt("_cleanliness", _cleanliness);
        }
        else
        {
            _cleanliness = PlayerPrefs.GetInt("_cleanliness");
        }

        if (!PlayerPrefs.HasKey("_iq"))
        {
                //_iq = 20;
                PlayerPrefs.SetInt("_iq", _iq);
        }
        else
        {
            _iq =PlayerPrefs.GetInt("_iq");
        }

    }
    


    public int hunger
    {
        get { return _hunger; }
        set { _hunger = value; }
    }

    public int happiness
    {
        get { return _happiness; }
        set { _happiness = value; }
    }

    public int cleanliness
    {
        get { return _cleanliness; }
        set { _cleanliness = value; }
    }

    public int iq
    {
        get { return _iq; }
        set { _iq = value; }
    }

    public string name
    {
        get { return _name; }
        set { _name = value; }
    }

    public void updateHappiness(int i)
    {
        happiness += i;
        if (happiness > 100)
            happiness = 100;
        if (happiness < 0)
        {
            //happiness = 0;
            gameManager.GameOver();
        }
    }

    public void updateHunger(int i)
    {
        hunger += i;
        if (hunger > 100)
            hunger = 100;
        if (hunger < 0)
        {
            //hunger = 0;
            gameManager.GameOver();
        }

        print("hunger" + hunger);
    }

    public void updateCleanliness(int i)
    {
        cleanliness += i;
        if (cleanliness > 100)
            cleanliness = 100;
        if (cleanliness < 0)
        {
            //cleanliness = 0;
            gameManager.GameOver();
        }

    }

    public void updateIQ(int i)
    {
        iq += i;
        if (iq > 100)
            iq = 100;
        if (iq < 0)
        {
            //iq = 0;
            gameManager.GameOver();
        }
    }

}
